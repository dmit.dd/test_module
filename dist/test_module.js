'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var url = require('url');

var query = {};

query.add = function (urlProps, paramName, paramValue) {
	/*
  * @param {Object} urlProps
  * @param {String} paramName
  * @param {String|Number|Boolean} paramValue
  * @returns {Object}
  */

	if ((typeof urlProps === 'undefined' ? 'undefined' : _typeof(urlProps)) != 'object' || urlProps === null) throw new TypeError('Parameter "urlProps" must be a object');

	if (typeof paramName != 'string') throw new TypeError('Parameter "urlString" must be a string');

	if (typeof paramValue != 'string' && typeof paramValue != 'number' && typeof paramValue != 'boolean') throw new TypeError('Parameter "paramValue" must be a string or number or boolean');

	if (_typeof(urlProps.query) == 'object' && typeof urlProps.query[paramName] != 'undefined') throw new Error('Param ' + paramName + 'already exist in url object');

	if (_typeof(urlProps.query) != 'object') urlProps.query = {};

	urlProps.query[paramName] = paramValue;
	return urlProps;
};

query.set = function (urlProps, paramName, paramValue) {
	/*
  * @param {Object} urlProps
  * @param {String} paramName
  * @param {String|Number|Boolean} paramValue
  * @returns {Object}
  */

	if ((typeof urlProps === 'undefined' ? 'undefined' : _typeof(urlProps)) != 'object' || urlProps === null) throw new TypeError('Parameter "urlProps" must be a object');

	if (typeof paramName != 'string') throw new TypeError('Parameter "urlString" must be a string');

	if (typeof paramValue != 'string' && typeof paramValue != 'number' && typeof paramValue != 'boolean') throw new TypeError('Parameter "paramValue" must be a string or number or boolean');

	if (typeof urlProps.query == 'undefined' || typeof urlProps.query[paramName] == 'undefined') throw new Error('Param ' + paramName + 'not found in url object');

	urlProps.query[paramName] = paramValue;
	return urlProps;
};

query.remove = function (urlProps, paramName) {
	/*
  * @param {Object} urlProps
  * @param {String} paramName
  * @returns {Object}
  */

	if ((typeof urlProps === 'undefined' ? 'undefined' : _typeof(urlProps)) != 'object' || urlProps === null) throw new TypeError('Parameter "urlProps" must be a object');

	if (typeof paramName != 'string') throw new TypeError('Parameter "urlString" must be a string');

	if (typeof urlProps.query == 'undefined' || typeof urlProps.query[paramName] == 'undefined') throw new TypeError('Param ' + paramName + 'not found in url object');

	delete urlProps.query[paramName];
	return urlProps;
};

function parseAuth(authString) {
	if (typeof authString != 'string') return null;

	var _authString$split = authString.split(':'),
	    _authString$split2 = _slicedToArray(_authString$split, 2),
	    user = _authString$split2[0],
	    password = _authString$split2[1];

	return {
		user: user,
		password: password
	};
}

function parseQuery(queryString) {
	if (typeof queryString != 'string') return null;

	var queryObj = {};
	var params = queryString.split('&');

	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = params[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			_param = _step.value;

			var _param$split = _param.split('='),
			    _param$split2 = _slicedToArray(_param$split, 2),
			    _param = _param$split2[0],
			    value = _param$split2[1];

			queryObj[_param] = value;
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	return queryObj;
}

function parse(urlString) {
	/*
  * @param {String} urlString
  * @returns {Object}
  */
	if (typeof urlString != 'string') throw new TypeError('Parameter "urlString" must be a string');

	var parsedUrl = url.parse(urlString);

	return {
		protocol: parsedUrl.protocol,
		auth: parseAuth(parsedUrl.auth),
		hostname: parsedUrl.hostname,
		port: parsedUrl.port,
		pathname: parsedUrl.pathname,
		query: parseQuery(parsedUrl.query),
		hash: parsedUrl.hash
	};
}

function formatAuth(authProps) {
	if ((typeof authProps === 'undefined' ? 'undefined' : _typeof(authProps)) != 'object' || authProps === null || typeof authProps.user != 'string' || typeof authProps.password != 'string') return null;

	return authProps.user + ':' + authProps.password;
}

function formatQuery(queryProps) {
	if ((typeof queryProps === 'undefined' ? 'undefined' : _typeof(queryProps)) != 'object' || queryProps === null) return null;

	var params = [];

	for (var _param2 in queryProps) {
		params.push(_param2 + '=' + queryProps[_param2]);
	}if (!params.length) return null;

	return params.join('&');
}

function format(urlProps) {
	/*
  * @param {Object} urlProps
  * @returns {String}
  */
	if ((typeof urlProps === 'undefined' ? 'undefined' : _typeof(urlProps)) != 'object' || urlProps === null) throw new TypeError('Parameter "urlProps" must be a object');

	var formattedQuery = formatQuery(urlProps.query);

	return url.format({
		protocol: urlProps.protocol,
		auth: formatAuth(urlProps.auth),
		hostname: urlProps.hostname,
		port: urlProps.port,
		pathname: urlProps.pathname,
		search: typeof formattedQuery == 'string' ? '?' + formattedQuery : null,
		query: formattedQuery,
		hash: urlProps.hash
	});
}

exports.parse = parse;
exports.format = format;
exports.query = query;