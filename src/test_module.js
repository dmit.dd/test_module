const url = require('url')

const query = {}

query.add = function (urlProps, paramName, paramValue)
{
	/*
	 * @param {Object} urlProps
	 * @param {String} paramName
	 * @param {String|Number|Boolean} paramValue
	 * @returns {Object}
	 */

	if (typeof urlProps != 'object' || urlProps === null)
		throw new TypeError('Parameter "urlProps" must be a object')

	if (typeof paramName != 'string')
		throw new TypeError('Parameter "urlString" must be a string')

	if (typeof paramValue != 'string' && typeof paramValue != 'number' && typeof paramValue != 'boolean')
		throw new TypeError('Parameter "paramValue" must be a string or number or boolean')

	if (typeof urlProps.query == 'object' && typeof urlProps.query[paramName] != 'undefined')
		throw new Error('Param ' + paramName + 'already exist in url object')

	if (typeof urlProps.query != 'object')
		urlProps.query = {}

	urlProps.query[paramName] = paramValue
	return urlProps
}

query.set = function (urlProps, paramName, paramValue)
{
	/*
	 * @param {Object} urlProps
	 * @param {String} paramName
	 * @param {String|Number|Boolean} paramValue
	 * @returns {Object}
	 */

	if (typeof urlProps != 'object' || urlProps === null)
		throw new TypeError('Parameter "urlProps" must be a object')

	if (typeof paramName != 'string')
		throw new TypeError('Parameter "urlString" must be a string')

	if (typeof paramValue != 'string' && typeof paramValue != 'number' && typeof paramValue != 'boolean')
		throw new TypeError('Parameter "paramValue" must be a string or number or boolean')

	if (typeof urlProps.query == 'undefined' || typeof urlProps.query[paramName] == 'undefined')
		throw new Error('Param ' + paramName + 'not found in url object')

	urlProps.query[paramName] = paramValue
	return urlProps
}

query.remove = function (urlProps, paramName)
{
	/*
	 * @param {Object} urlProps
	 * @param {String} paramName
	 * @returns {Object}
	 */

	if (typeof urlProps != 'object' || urlProps === null)
		throw new TypeError('Parameter "urlProps" must be a object')

	if (typeof paramName != 'string')
		throw new TypeError('Parameter "urlString" must be a string')

	if (typeof urlProps.query == 'undefined' || typeof urlProps.query[paramName] == 'undefined')
		throw new TypeError('Param ' + paramName + 'not found in url object')

	delete urlProps.query[paramName]
	return urlProps
}

function parseAuth (authString)
{
	if (typeof authString != 'string')
		return null

	let [user, password] = authString.split(':')

	return {
		user: user,
		password: password
	}
}

function parseQuery (queryString)
{
	if (typeof queryString != 'string')
		return null

	let queryObj = {}
	let params = queryString.split('&')

	for (param of params)
	{
		let [param, value] = param.split('=')
		queryObj[param] = value
	}

	return queryObj
}

function parse (urlString)
{
	/*
	 * @param {String} urlString
	 * @returns {Object}
	 */
	if (typeof urlString != 'string')
		throw new TypeError('Parameter "urlString" must be a string')

	let parsedUrl = url.parse(urlString)

	return {
		protocol: parsedUrl.protocol,
		auth: parseAuth(parsedUrl.auth),
		hostname: parsedUrl.hostname,
		port: parsedUrl.port,
		pathname: parsedUrl.pathname,
		query: parseQuery(parsedUrl.query),
		hash: parsedUrl.hash
	}
}

function formatAuth (authProps)
{
	if (typeof authProps != 'object' || authProps === null || typeof authProps.user != 'string' || typeof authProps.password != 'string')
		return null

	return `${authProps.user}:${authProps.password}`
}

function formatQuery (queryProps)
{
	if (typeof queryProps != 'object' || queryProps === null)
		return null

	let params = []

	for (let param in queryProps)
		params.push(`${param}=${queryProps[param]}`)

	if (!params.length)
		return null

	return params.join('&')
}

function format (urlProps)
{
	/*
	 * @param {Object} urlProps
	 * @returns {String}
	 */
	if (typeof urlProps != 'object' || urlProps === null)
		throw new TypeError('Parameter "urlProps" must be a object')

	let formattedQuery = formatQuery(urlProps.query)

	return url.format({
		protocol: urlProps.protocol,
		auth: formatAuth(urlProps.auth),
		hostname: urlProps.hostname,
		port: urlProps.port,
		pathname: urlProps.pathname,
		search: typeof formattedQuery == 'string' ? '?' + formattedQuery : null,
		query: formattedQuery,
		hash: urlProps.hash
	})
}

export { parse, format, query }