const chai = require('chai')
const assert = chai.assert
const expect = chai.expect
const test_module = require('./../dist/test_module')

describe('test_module', () =>
{
	let inputUrlString = 'https://user1:password1@www.example.com:8080/sub1/sub2/sub3/sub4?par1=val1&par2=val2&par3=val3#hash'

	describe('parse', () =>
	{
		let urlObj = test_module.parse(inputUrlString)

		it('invalid urlString', () =>
		{
			expect(test_module.parse).to.throw(TypeError)
		})

		it('protocol', () =>
		{
			assert.equal('https:', urlObj.protocol)
		})

		it('auth', () =>
		{
			assert.deepEqual({ user: 'user1', password: 'password1' }, urlObj.auth)
		})

		it('hostname', () =>
		{
			assert.equal('www.example.com', urlObj.hostname)
		})

		it('port', () =>
		{
			assert.equal('8080', urlObj.port)
		})

		it('pathname', () =>
		{
			assert.equal('/sub1/sub2/sub3/sub4', urlObj.pathname)
		})

		it('query', () =>
		{
			assert.deepEqual({
				par1: 'val1',
				par2: 'val2',
				par3: 'val3'
			}, urlObj.query)
		})

		it('hash', () =>
		{
			assert.equal('#hash', urlObj.hash)
		})

		it('empty string', () =>
		{
			assert.deepEqual({
				protocol: null,
				auth: null,
				hostname: null,
				port: null,
				pathname: null,
				query: null,
				hash: null
			}, test_module.parse(''))
		})

	})

	describe('query', () =>
	{
		it('add param', () =>
		{
			let urlProps = {
				hostname: 'example.com',
				query: {
					test: 1,
					test2: '2'
				}
			}

			test_module.query.add(urlProps, 'test3', false)
			assert.equal('example.com?test=1&test2=2&test3=false', test_module.format(urlProps))
		})

		it('add param invalid urlProps', () =>
		{
			expect(() => test_module.query.add(null, 'test3', false)).to.throw(TypeError)
			expect(() => test_module.query.add(undefined, 'test3', false)).to.throw(TypeError)
			expect(() => test_module.query.add(3, 'test3', false)).to.throw(TypeError)
		})

		it('add param invalid paramName', () =>
		{
			expect(() => test_module.query.add({}, null, false)).to.throw(TypeError)
			expect(() => test_module.query.add({}, undefined, false)).to.throw(TypeError)
			expect(() => test_module.query.add({}, 3, false)).to.throw(TypeError)
		})

		it('add param invalid paramValue', () =>
		{
			expect(() => test_module.query.add({}, 'test', null)).to.throw(TypeError)
			expect(() => test_module.query.add({}, 'test', undefined)).to.throw(TypeError)
			expect(() => test_module.query.add({}, 'test', {})).to.throw(TypeError)
			expect(() => test_module.query.add({}, 'test', ()=>{})).to.throw(TypeError)
		})

		it('add param already exist', () =>
		{
			expect(() => test_module.query.add({ query: { test: 1 }}, 'test', 1)).to.throw(Error)
		})

		it('set param value', () =>
		{
			let urlProps = {
				hostname: 'example.com',
				query: {
					test: 1,
					test2: '2'
				}
			}

			test_module.query.set(urlProps, 'test', false)
			assert.equal('example.com?test=false&test2=2', test_module.format(urlProps))
		})

		it('set param invalid urlProps', () =>
		{
			expect(() => test_module.query.set(null, 'test3', false)).to.throw(TypeError)
			expect(() => test_module.query.set(undefined, 'test3', false)).to.throw(TypeError)
			expect(() => test_module.query.set(3, 'test3', false)).to.throw(TypeError)
		})

		it('set param invalid paramName', () =>
		{
			expect(() => test_module.query.set({}, null, false)).to.throw(TypeError)
			expect(() => test_module.query.set({}, undefined, false)).to.throw(TypeError)
			expect(() => test_module.query.set({}, 3, false)).to.throw(TypeError)
		})

		it('set param invalid paramValue', () =>
		{
			expect(() => test_module.query.set({}, 'test', null)).to.throw(TypeError)
			expect(() => test_module.query.set({}, 'test', undefined)).to.throw(TypeError)
			expect(() => test_module.query.set({}, 'test', {})).to.throw(TypeError)
			expect(() => test_module.query.set({}, 'test', ()=>{})).to.throw(TypeError)
		})

		it('set param not found', () =>
		{
			expect(() => test_module.query.set({}, 'test', 1)).to.throw(Error)
		})

		it('remove par', () =>
		{
			let urlProps = {
				hostname: 'example.com',
				query: {
					test: 1,
					test2: '2'
				}
			}

			test_module.query.remove(urlProps, 'test')
			assert.equal('example.com?test2=2', test_module.format(urlProps))
		})

		it('remove param invalid urlProps', () =>
		{
			expect(() => test_module.query.remove(null, 'test3', false)).to.throw(TypeError)
			expect(() => test_module.query.remove(undefined, 'test3', false)).to.throw(TypeError)
			expect(() => test_module.query.remove(3, 'test3', false)).to.throw(TypeError)
		})

		it('remove param invalid paramName', () =>
		{
			expect(() => test_module.query.remove({}, null, false)).to.throw(TypeError)
			expect(() => test_module.query.remove({}, undefined, false)).to.throw(TypeError)
			expect(() => test_module.query.remove({}, 3, false)).to.throw(TypeError)
		})

		it('remove param not found', () =>
		{
			expect(() => test_module.query.remove({}, 'test', 1)).to.throw(Error)
		})
	})

	describe('format', () =>
	{
		it('invalid urlProps', () =>
		{
			expect(test_module.format).to.throw(TypeError)
		})

		it('protocol', () =>
		{
			assert.equal('http://example.com', test_module.format({
				protocol: 'http:',
				hostname: 'example.com'
			}))
		})

		it('auth', () =>
		{
			assert.equal('user1:password1@example.com', test_module.format({
				hostname: 'example.com',
				auth: { user: 'user1', password: 'password1' }
			}))
		})

		it('auth with out user', () =>
		{
			assert.equal('example.com', test_module.format({
				hostname: 'example.com',
				auth: { password: 'password1' }
			}))
		})

		it('auth with out password', () =>
		{
			assert.equal('example.com', test_module.format({
				hostname: 'example.com',
				auth: { user: 'user1' }
			}))
		})

		it('hostname', () =>
		{
			assert.equal('example.com', test_module.format({
				hostname: 'example.com'
			}))
		})

		it('port', () =>
		{
			assert.equal('example.com:8080', test_module.format({
				hostname: 'example.com',
				port: '8080'
			}))
		})

		it('pathname', () =>
		{
			assert.equal('example.com/sub1/sub2/sub3/sub4', test_module.format({
				hostname: 'example.com',
				pathname: '/sub1/sub2/sub3/sub4'
			}))
		})

		it('empty query', () =>
		{
			assert.equal('example.com', test_module.format({
				hostname: 'example.com',
				query: {}
			}))
		})

		it('query', () =>
		{
			let urlProps = {
				hostname: 'example.com',
				query: {
					test: 1,
					test2: '2'
				}
			}
			assert.equal('example.com?test=1&test2=2', test_module.format(urlProps))
		})

		it('hash', () =>
		{
			assert.equal('example.com#test', test_module.format({
				hostname: 'example.com',
				hash: '#test'
			}))
		})

		it('empty object', () =>
		{
			assert.equal('', test_module.format({}))
		})
	})
})
